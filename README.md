# A website for all (small) voting needs
This repository is intended to end in a website useful for carrying out voting in small groups, for whatever purpose the group needs. Additionally, there will be information about different topics related to voting, such as tactical voting and criteria.

The site should help the user select the most suitable voting method, based on their needs.
