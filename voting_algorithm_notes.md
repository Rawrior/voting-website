All voting methods are vulnerable to compromising

- Single-winner
	- Approval voting  
		All methods allow for bullet voting. They are immune to push-over and burying.
		1. Voters mark all candidates they approve of
		2. Winner is the candidate with highest final tally
		- Combined approval voting
			1. Voters mark candidates as either 'for', 'against', or 'indifferent' (no mark)
			2. Votes are tallied. 'For' counts as +1, 'against' counts as -1, 'indifferent' has no effect
			3. Winner is the candidate with highest final tally
		- Unified primary
			1. Apply approval voting
			2. Apply 1 extra round (runoff) with only the top 2 candidates.
			3. Winner is the candidate in the runoff with the highest final tally
	- Borda count  
	Allows for burying, bullet-voting. Immune to push-over?
		1. Voters rank any number of candidates according to preference (1 to n)
		2. For n candidates, a ballot will yield n - 1 points to the top candidate, n - 2 to the second-highest, and so on. Candidates with no ranking on the ballot get 0 points
			- Alternatively, ranks can yield n, n - 1, ..., 1 points.
			- Alternatively, ranks can yield (1/rank) points
		3. Winner is the candidate with highest total tally
	- Bucklin voting  
	Allows for bullet voting
		1. Voters rank any number of candidates according to preference
		2. Only first-choice votes are counted
		3. If there is no majority, the next tier of rankings are added, still counting as 1 vote
		4. Step 3 repeats until there is a majority winner
	- Contingent vote
		1. Voters rank any number of candidates according to preference
		2. If there is no majority winner, the two candidates with the most first-rank choices are selected for runoff
		3. Remaining votes are transferred to whichever of the two candidates the voter then prefers higher
		4. Winner is the candidate with the larger majority
		- Supplementary vote
			1. Voters rank only their first and second preference
			2. Contingent vote is then applied
			3. In runoff voting, second choice is only counted if one of the two remaining candidates
	- Condorcet methods  
	Only vulnerable to compromising if there is a cycle, or one can be created. All condorcet methods are vulnerable to burying.
		1. Voters rank any number of candidates according to preference
		2. For each ballot, a matrix is created (runner as rows, opponent as columns), where a runner is given a score of 1 if they beat the opponent
		3. All matrices for all ballots are then summed together
		4. Winner is the candidate that beats all other candidates.
		5. If no winner is found, other methods must be applied to find the winner.
		- Copeland's method
			1. Voters rank any number of candidates according to preference
			2. A result matrix is created. Element result_ab is...
				- 1 if a majority of voters prefers candidate a to candidate b
				- 1/2 if there is a tie
				- 0 if a majority of voters prefer candidate b to candidate a
			3. Winner is the candidate that beats all other candidates
			4. If not winner is found, other methods must be applied to find the winner.
		- Dodgson's method  
		(NP-hard to compute, maybe don't implement)
			1. Do normal condorcet voting
			2. If there is no condorcet winner, compute the minimum number of swaps over all ballots to make each candidate condorcet winner
			3. The winner is the candidate that required the least number of swaps.
		- Kemeny-Young method  
		(Also NP-hard to compute (factorial time with number of candidates), maybe don't implement for more than x choices)
			1. Voters rank any number of candidates according to preference (possibly with some candidates at equal level)
			2. Each ballot creates a tally table, where each pair of candidates is given a vote in "A over B", "B over A", or "no preference"
			3. All tally tables are then summed together
			4. Each ranking permutation of candidates uses the tally table to determine its score. For a given pairing, the tally that prefers A over B is added to the permutations score
			5. The ranking permutation with the highest score is the winner
		- Minimax condorcet  
		(Not quite understanding this one)
			1. Voters rank all/any number of candidates according to preference
			2. with score(A, B) denoting the pairwise score for A against B (how many voters across all ballots prefer A over B?), the winner of the pairing is found by min(max( score(B, A))) (???)
			3. The candidate that beats all other candidates is the condorcet winner.
				- In case equal rankings or not ranking all candidates is permitted, different criteria may be used to determine the winner. This is either winning votes, margins, or pairwise opposition comparisons
		- Nanson's method
		(Can also be adapted to handle bullet votes and equal rankings)
			1. Voters rank any number of candidates according to preference
			2. A borda count of the ballots is conducted
			3. Candidates that are at or below the average Borda score are eliminated
			4. Step 2 and 3 repeat until a single winner remains
				- This may also be used to find multiple winners, though this will simply find the n highest ranked candidates, and is not proportional.
		- Baldwin's method
			1. Nanson's method, but only the candidate with the lowest points is eliminated each round
		- Ranked pairs
		(Can also be used to create a sorted list of winners)
			1. Voters rank all candidates according to preference
			2. Compare each pair of candidates and determine the strength of victory (usually margin of victory) for each candidate
				1. Count all ballots where A > B (Awin)
				2. Count all ballots where B > A (Bwin)
				3. A's margin of victory is Awin - Bwin, while B's margin of victory is Bwin - Awin
			3. "Lock in" each pair, starting with the largest strength of victory, by adding the pair to a graph, with the existing locked pairs, if it does not create a cycle.
			4. The completed graph shows the winner, the source of the graph
				- If there are ties, a random choice can be made. Tideman has a considerably more complicated procedure, which is outlined in his paper
		- Schulze method
			1. Voters rank any number of candidates according to preference. 
			2. Let d(A, B) be the number of voters who prefer A to B
			3. A path from candidate X to Y is a sequence of candidates C_1, ..., C_n with the following properties:
				- C_1 = X and C_n = Y
				- For all i = 1, ..., n-1 : d(C_i, C_i+1) > d(C_i+1, C_i)
				- (I.E. In pairwise comparison, each candidate in the path will beat the following candidate)
			4. The strength p of a path from X to Y is the smallest number of voters in the sequence.
				- For example, if the number of coters in the path go 5-8-3-6, the strength is 3
			5. The winner is the candidate A with p(A, X) >= p(X, A) for every other candidate X
	- Exhaustive ballot
		1. Voters vote for a single candidate
		2. If no candidate is supported by a majority of votes, the candidate with the fewest votes is eliminated
		3. Steps 1 and 2 repeat until there is a majority winner
	- First-past-the-post voting
		1. Voters vote for a single candidate
		2. The candidate who receives the most votes win
	- Instant-runoff voting
		1. Voters rank any number of candidates according to preference. No candidates may have equal rank
		2. If a candidate has more than half of the vote, they win.
		3. If there is no winner, the candidate with the least votes is eliminated
		4. The voters whose highest-choice candidate was eliminated, have their votes added to the total of their next-highest-choice candidate
		5. Steps 2-4 repeat until a candidate has more than half the vote.
		- Coombs' method
			1. Voters rank all candidates according to preference
			2. If a candidate has a majority of first-choice votes, the candidate wins
			3. Otherwise, the candidate with the largest number of last-choice votes is eliminated
			4. Votes for the eliminated candidate are redistributed to the next-to-last choice, according to rank
			5. Steps 2-4 are repeated until a winner is found
	- Majority judgment
		1. Voters grade any number of candidates according to their suitability for office: Excellent, very good, good, acceptable, poor, or reject. Multiple candidates may be given the same grade
		2. The candidate with the highest median grade wins
			1. Place all grades, high to low, top to bottom, side-by-side with each candidate. Find the median grade
			2. If there is a tie, remove one-by-one any votes equal to the median grade from each tied candidate's total.
			3. Repeat until one candidate has the highest median grade.
	- Score voting
		1. Voters score all candidates according to preference, on some given scale (0-10, 0-100, others)
		2. The candidate with the highest sum score or highest average score is the winner
	- STAR voting
		1. Voters rank all candidates according to preference, on a 0-5 scale.
		2. The two candidates with the highest rating become finalists
		3. The candidate with more preferred votes (I.E. rated higher than opponent on all ballots) wins
	- Two-round system
		1. Voters vote for their preferred candidate
		2. If a candidate has a majority of votes, they are the winner. If not, the top two candidates proceed to another round of voting
		3. Voters choose which of the two options the prefer.
		4. The candidate with the majority of votes win.
	- Usual judgment
		1. Voters rank all candidates on a scale: excellent, very good, good, fair, passable, inadequate, bad. Non-evaluation defaults to a bad rating
		2. Each candidate has a majority grade. When the number of electors is uneven (2n + 1), it is the grade given by elector n + 1 from the lowest grade. When even, it is the elector n.
		3. The candidate with the highest grade that no other candidate also has, is the winner
		4. In the case of a tie, each candidate goes through the following formula: a_c + (1/2 * (p_c - q_c)/(1 - p_c - q_c)), where a_c is the majority grade for the candidate, p_c is the share of voters giving the candidate a strictly higher grade than the majority grade, and q_c is the share of voters giving the candidate a stricly lower grade than the majority grade
		5. The winner is the candidate whose result from this formula is highest. If there is still a tie, additional tie-breaking is completed as follows:
		6. p_c^n is the share of voters who have given a grade superior or equal to a_c + n, while q_c^n is the share of voters who have given a grade inferior or equal to a_c - n. For example, if the majority grade is 'fair', p_c^1 is all votes up to excellent. p_c^2 are all votes that are excellent or very good. p_c^3 are only the votes for excellent. Same thing in reverse for q_c^n. Each time, the formula for step 4 is repeated, but p_c and q_c is replaced by p_c^n and q_c^n
		7. If there is still a tie, the candidates are ranked according to the lexicographic order of their vector (-q_c, p_c, -q_c^2, p_c^2, ..., -q_c^G, p_c^G), where G is the number of possible grades.
		8. If there is still a tie, the candidates have received exactly the same votes, "an extremely improbable situation"
- Proportional
	- CPO-STV (Comparison of Pairs of Outcomes by the Single Transferable Vote)
		1. Voters rank candidates according to preference. Precise rules for the given CPO-STV determine whether they must rank all candidates, and/or whether candidates may receive equal ranks
		2. The 'quota' is set to (total votes / (seats + 1))
		3. All possible outcomes of the election are then generated, to be compared to each other outcome
			- Note: Outcomes that are impossible, based on the votes given, can be eliminated to reduce the amount of outcomes needing to be compared
		4. The comparison is done as follows:
			1. Eliminate candidates in neither outcome. Votes for the candidate are transferred to each ballot's next most preferred candidate who is present in at least 1 of the outcome
			2. Transfer surpluses of candidates in both outcomes. Votes above the quota are transferred to the voters next most preferred candidate, that is present in at least 1 of the outcomes. NOTE: Only surpluses of candidates who are in both outcomes are transferred. Other surpluses are ignored
				- Transfer of surpluses can be done with various methods such as Warren's or Meek's method.
			3. Add up the totals of the outcome. The total number of votes cast for the candidates in the outcome is the score
			4. The highest score between the two outcomes is the winner
		5. The outcome that beats all others is the winner
		6. If there is no clear winner, a condorcet completion method must be used, such as the one for ranked pairs or the Schulze method
	- Dual member
		- Elects specifically 2 representatives for each of a region's districts
		1. Voters receive a district-specific ballot. Each option is either a pair of ranked candidates (primary and secondary) with the same party, a single candidate with a party, or an independent
		2. Voters select their preferred option.
		3. Seats are allocated to parties, based on the largest remainder method with Hare quote equal to twice the number of districts
		4. 1 seat in every district is awarded based on FPTP voting. If the winner of the seat is from a party with two candidates in the district, the votes transfer at half value.
		5. If the remaining candidate with the highest vote share in any district is an independent, they are elected, and all other independent candidates are eliminated
		6. Parties are now missing n seats, where n = number of seats the party should have (step 3) - number of seats the party has already won (step 4)
		7. Make a ranked list of each party's candidates, ranked by highest fraction of votes in their district
		8. Assign the top n party candidates to their secondary district seats
		9. If more than 1 candidate (from different parties) have been assigned the same district seat, assign the party candidate that did best between them. 
		10. Award the losing party the next highest seat on their list
		11. Repeat steps 9 and 10 until there are no conflicts
	- Hare-Clark
		1. Voters rank any number of candidates according to preference
		2. Voters first preference votes are tallied
		3. The droop quota is used to see if any candidates are immediately elected. The quota is (Total number of valid votes / (number of seats + 1)) + 1
		4. Candidates with surplus have their transfer value determined: (surplus votes/last votes received) (The last votes received are for any. In the first round, it would be the direct votes. In the second round (and so one), it is the votes received from surplus)
		5. Votes are transferred to the next preference on the ballot, at the transfer value from the elected candidate
		6. Votes are re-tallied with the transferred surplus votes
		7. When all surpluses have been transferred, and there are still available seats, the candidate with the lowest amount of votes is eliminated.
		8. Votes from the eliminated candidate are transferred to their next preference, and the process returns to step 3.
	- Highest average method
		1. Voters cast a single vote for their preferred candidate. For a party p, their entitlement to seats t_i is (votes received / total number of votes)
		2. Iteratively, seats are assigned to a party using a divisor method given by a function d(k), mapping each integer k to a real number (usually in the range [k, k+1])
		3. The number of seats s given to a party p is denoted s_p. At each iteration, the next seat allocated goes to the party that maximizes the ration (t_i / d(s_p)). This continues until all seats are assigned
			- Specific divisor methods can be found below:
		- Webster/Sainte-Laguë
			- The divisor function is given by d(k) = k + 0,5
		- D'Hondt
			- The divisor function is given by d(k) = k + 1
		- Imperiali
			- The divisor function is given by d(k) = k + 2
		- Huntington-Hill
			- The divisor function is given by d(k) = sqrt(k*(k+1))
		- Danish
			- The divisor function is given by d(k) = k + 1/3
		- Adams'
			- The divisor function is given by d(k) = k
		- Dean's
			- The devisor function is given by d(k) = k*(k + 1) / (k + 0.5) = 2/ (1/k + 1/(k+1)
	- Largest remainder method
		1. Voters cast a single vote for their preferred candidate
		2. The amount of votes are then divided by some quota representing the number of votes required for a seat. This leaves each party with fractional number.
		3. Parties are assigned seats equal to the integer part of the fractional number they receive
		4. Remaining seats are assigned in order to the largest remainders of each party's fractional number
			- Quotas that can be used:
				- Hare: (total votes / total seats)
				- Droop: floor(1 + (total votes / (1 + total seats)))
				- Hagenbach-Bischoof: (total votes / (1 + total seats))
				- Imperiali: (total votes / (2 + total seats))
	- Mixed-member proportional representation
		1. Voters vote for a single candidate for their district, and a single political party nationally
		2. Each district uses a single-winner method (usually FPTP) to determine the winning candidate for the district
		3. Parties are assigned a proportional share of seats in parliament, based on the number of votes received in the party portion of the ballot
		4. The number of district seats that the party has won, is subtracted from their share, as they are already won
		5. Remaining seats are then assigned by using the largest remainder method, or the D'Hondt or Sainte-Laguë method
		6. If a party wins more single-candidate seats than their party votes would delegate, they may be kept. Commonly, these are added as extra seats in the parliament, so that propotionality is maintained
		- Additional member
			1. Voting happens according to the MMPR method
			2. When allocating missing seats using for example D'Hondt, the seats already won are taken into account (if a party won 5 seats, D'Hondt would start at 6, not 1)
	- Mixed single vote
		1. MMPR, but votes for a candidate also vote for the party they are affiliated with
	- Party-list
		1. Voters vote for a single candidate or a single party
		2. The order in which a party's candidates are elected may be decided by the party (closed list), or voters are large (open list), or by districts (local list)
		3. Seats are awarded either with highest averages method, or largest remainder method
	- Proportional approval voting
		1. Voters vote their approval for any number of candidates
		2. To calculate the score, generate all possible combinations of elected candidates W
		3. For each combination V in W, a ballot contributes (1 + 1/2 + 1/3 + ... + 1/r) points, for r candidates they approve of, that are in the combination (such that, if there is only 1 candidate, the ballot gives 1 point, for 2 it gives 1 + 1/2, and so on)
		4. The combination with the highest score is elected
	- Sequential proportional approval voting
		The formula in step 2 is based on D'Hondt, but other weighting formulas can be used as well
		1. Voters vote their approval for any number of candidates
		2. Each ballot contributes (1 / (s + 1)) score to a candidate, where s is the number of candidates on the ballot already elected
		3. The candidate with the highest score is elected
		4. Steps 2 and 3 repeat until there are no more open seats
	- Schulze STV
		1. Voters rank any number of candidates according to preference
		2. Any candidates with more votes than the D'Hondt quotient (valid votes / (seats to fille + 1)) are elected
		3. Surplus votes are transferred to a ballot's next highest choice. Surplus votes count for ((votes next preferring this candidate / total votes for original candidates) * surplus votes for original candidate)
		4. Steps 2 and 3 are repeated until all seats are filled
	- Single transferable vote
		1. Voters rank any number of candidates according to preference
		2. Any candidates with more votes than the quotient selected for the election are elected
		3. Surplus votes for the candidate are transferred to the next preference of the ballot, at full value
		4. Steps 2 and 3 repeat until all seats are filled
		5. If step 2 does not apply, the least popular candidate is eliminated, and the votes for the candidate are distributed according to next preference of the ballots. 
		6. Step 5 is repeated until step 2 can apply again
	- Spare vote
		1. Voters rank 2 (or however many spare votes are allowed) candidates according to preference
		2. For one-step procedure, all votes first-preference for parties that are below the electoral threshhold are discarded. The second-preference votes are then counted instead
		3. For multi-round procedure, the party with the least number of first-preference votes is eliminated, and the second-preference votes are put in effect. This repeats until there are no parties under the election treshhold
		4. Votes are then used in another system to elect winning parties
	- Indirect single transferable voting
		1. STV, but transfer of votes happen according to a candidate's instructions
- Semi-proportional
	- Parallel voting
		1. Voters use 2 or more election systems to elect a number of candidates. Each system elects some proportion of available seats
	- Single non-transferable vote
		1. FPTP voting, but in multiple districts
	- Cumulative voting
		1. Voters vote for any number of candidates, depending on the specific system used
			- Equal and even voting: Voters have 1 vote, which they can divide evenly between up to n candidates
			- Points voting: Voters are given n points, which they can divide between any number of candidates
			- Fractional voting: Voters have 1 vote, which they can divide, according to preference, between up to n candidates
		2. Voting then proceeds with some other system
	- Limited voting
		1. Voters are given n votes to elect k (k > n) seats. Voters vote for their preferred candidates
		2. The n candidates with the most votes are elected.
	- Satisfaction approval voting
		1. Voters indicate their approval of n candidates
		2. Approval votes are counted normally, but a vote on a ballot only counts for 1/n points
		3. The candidates with the highest approvals win
	- Additional member system
		1. See the same in proportional systems
	- Mixed single vote
		1. MMPR, but split tickets are not allowed. Votes for a candidate are also votes for the party
	- Alternative vote plus
		1. Voters vote as in IRV, for part of the parliament
		2. A secondary vote is held for regional party lists (Party-list in proportional systems), for the remaining part of parliament
	- Dual-member
		- See dual member in proportional systems
	- Rural-urban
		1. Areas are divided into 'rural' and 'urban' areas
		2. Rural and urban areas use different methods of voting
		3. Rural votes will elect some part of parliament, while urban votes will elect the remaining part

Types of tactical voting:
- Compromising
	- Voting for candidate that the voter thinks has a higher chance of winning, rather than the candidate they truly prefer
- Burying
	- Ranking a non-preferred alternative lower, with the voter hoping that the alternative will have a lower chance of winning
- Push-over
	- Ranking a (possibly) weak alternative higher, in the hopes that the weak alternative will beat a less-liked alternative, and in later rounds go on to lose to the voter's actually preferred candidate.
- Bullet voting
	- Voting for only a single candidate, when it's possible to vote for multiple, such that rival candidates get less votes.

Criteria:
- Condorcet winner criterion
	- The election system always chooses the condorcet winner, if such a winner exists
	- The condorcet winner is the candidate that beats all other candidates in pairwise comparisons
	- Often simply called the condorcet criterion
	- A condorcet winner may not exist for a given election
		- There will, however, always be a set of candidates such that voters prefer anyone in the set, over anyone not in the set. This is the Smith set.
- Condorcet loser criterion
	- The election system will never allow a condorcet loser to win.
	- A condorcet loser is a candidate who loses in a pairwise comparison against each other candidate
	- A condorcet loser may not exist
- Consistency criterion
	- If an election system, when an electorate is divided arbitrarily into several parts and elections in those parts garner the same result, also garners the same result for the full electorate, then the system is consistent
	- This can only happen in a ranked voting system, if the system is a scoring function
		- Such as positional voting or Borda count
- Independence of clones
	- An election system fulfills independence of clones, if the winner of the election does not change when a non-winning candidate similar to another candidate is introduced
	- Systems that fail this criterion can be clone negative or clone positive, or neither
		- Clone negative means that the addition of a similar candidate decreases another candidate's chance of winning
		- Clone positive means that the addition of a similar candidate increases another candidate's chance of winning
		- If neither, it is because the system chooses a different winner when a non-winning clone is introduced, but the new winner is not the candidate that was cloned. This is called crowding
- Independence of irrelevant alternatives
	- An election system fulfills independence of irrelevant alternatives, if, when introducing a new (irrelevant) candidate, a voter will not switch away from their more-liked option.
		- For example, in a race between A and B, where A is better liked, introducing candidate C should not cause voters to switch their votes from A to B
	- There is also local independence (of irrelevant alternatives). In this, the following are required:
		- If an option that finished last place is deleted from all the votes, the order of finish for the remaining options must not change, I.E. the winner must not change
		- If the winning option is deleted from all votes, the order of finish of the remaining options must not change, I.E. the second place must now be the winner
- Indepencence of Smith-dominated alternatives
	- AKA Weak independence of irrelevant alternatives
	- An election system fulfills independence of Smith-dominated alternatives, if the selection of the winner is independent of candidates who are not within the Smith set.
		- In short: If all candidates in the Smith set are eliminated, the outcome of the election does not change
	- Fulfillment of this criterion implies fulfillment of the Smith criterion
- Later-no-harm criterion
	- An election system fulfills later-no-harm if adding more (lower) preferences do not reduce the likelihood of a higher preference from being elected
- Later-no-help criterion
	- An election system fulfills later-no-help if adding more (higher) preferences can not cause a more-preferred candidate to win
- Majority criterion
	- If one candidate is ranked first by a majority of voters, that candidate must win
- Majority loser criterion
	- If a majority of voters prefer every other candidate over a given candidate, that candidate must not win
- Monotonicity criterion
	- An election system is monotonic if it is not possible to prevent the election of a candidate by ranking them higher on some ballots, nor possible to elect an otherwise unelected candidate by ranking them lower on some ballots
- Mutual majority criterion
	- If there is a subset S of the candidates, such that more than half the voters strictly prefer every candidate in S to every candidate outside of S, the winner must come from S
- Pareto efficiency
	- AKA Pareto optimality
	- No candidate can be given a better result, without giving a different candidate a worse result
	- I.E. if every voter prefers X over Y, the election system prefers X over Y as well
- Participation criterion
	- Different for deterministic and probabilistic frameworks
		- In a deterministic framework, the addition of a ballot where A is strictly preferred over B, should not change the winner from A to B
		- In a probabilistic framework, the addition of a ballot where each candidate of a set X is preferred over every other candidate, should not reduce the probability that the winner is chosen from the set X
- Plurality criterion
	- If the number of ballots ranking A as first preference is greater than the number of ballots on which another candidate B is given any preference, then A's probablity of winning must be no lass than B.
- Resolvability criterion
	- Either
		- For every (possibly tied) winner in a result, there must exist a way for one added vote to make that winner unique
		- The proportion of outcoes giving a tie approaches 0 as the number of voters increase towards infinity
- Reversal symmetry
	- If A is the unique winner in the system, then A must not be elected if each voter's preferences are inverted
- Smith criterion
	- The election system elects a winner from the Smith set
